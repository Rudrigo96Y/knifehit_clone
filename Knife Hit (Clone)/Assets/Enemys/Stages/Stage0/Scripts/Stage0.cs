﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage0 : MonoBehaviour {

    private AudioSource _myAudioSource;

    public int knife2Unlock = -1;
    /// <summary>
    /// Esse é o avlo.
    /// </summary>
    [SerializeField]
    private float _rotateSpeed = 150f;
    private float __rotateSpeedBackup;
    private float _rotateSpeedValue = 150f;
    public float aceleration = 100;
    [SerializeField]
    private Vector2 _time2ChangeDir;
    private float _timer2ChangeDir;
    [SerializeField]
    private Vector2 _time2StopMove;
    private float _timer2StopMove;
    [SerializeField]
    private Vector2 _time2StayStop;
    private float _timer2StayStop;

    public SpriteRenderer spriteRenderer;
    public Transform itensPos;
    public Rigidbody2D[] parts;

    [Header("Explodir")]
    [SerializeField]
    private float _woodBlewUpForce = 3;
    [SerializeField]
    private float _woodTorqueForce = 5;
    [SerializeField]
    private float _knifeBlewUpForce = 5;
    [SerializeField]
    private float _knifeTorqueForce = 7;

    private void Awake() {
        _myAudioSource = GetComponent<AudioSource>();
    }

    void FixedUpdate() {
        _rotateSpeedValue = Mathf.Lerp(_rotateSpeedValue, _rotateSpeed, aceleration * Time.fixedDeltaTime);
        transform.Rotate(0, 0, _rotateSpeedValue * Time.fixedDeltaTime);

        //Muda a direção que esta rodando
        if (_time2ChangeDir.x > 0 && _time2ChangeDir.y > 0) {
            _timer2ChangeDir -= Time.fixedDeltaTime;

            if (_timer2ChangeDir < 0) {
                _rotateSpeed *= -1;
                _timer2ChangeDir = Random.Range(_time2ChangeDir.x, _time2ChangeDir.y);
            }
        }

        //Faz o personagem parar de rodar
        if (_time2StopMove.x > 0 && _time2StopMove.y > 0) {
            _timer2StopMove -= Time.fixedDeltaTime;

            if (_timer2StopMove < 0f) {
                _rotateSpeed = 0;

                _timer2StayStop -= Time.fixedDeltaTime;
                if (_timer2StayStop < 0) {
                    _timer2StopMove = Random.Range(_time2StopMove.x, _time2StopMove.y);
                    _timer2StayStop = Random.Range(_time2StayStop.x, _time2StayStop.y);
                    _rotateSpeed = __rotateSpeedBackup;
                }
            }
        }
    }

    public void BlewUp() {
        if (!_myAudioSource.isPlaying) {
            _myAudioSource.Play();

            //Quebra a madeira.
            spriteRenderer.enabled = false;
            enabled = false;
            for (int i = 0; i < parts.Length; i++) {
                parts[i].gameObject.SetActive(true);
                parts[i].AddForce(parts[i].transform.up * Random.Range(-_woodBlewUpForce, _woodBlewUpForce), ForceMode2D.Impulse);
                parts[i].AddTorque(Random.Range(-_woodTorqueForce, _woodTorqueForce), ForceMode2D.Impulse);
            }
            //Faz as facas grudadas cairem.
            for (int i = 0; i < itensPos.childCount; i++) {
                Rigidbody2D tp_Rigidbody2D = itensPos.GetChild(i).GetComponent<Rigidbody2D>();
                tp_Rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
                Destroy(tp_Rigidbody2D.GetComponent<BoxCollider2D>());
                tp_Rigidbody2D.AddForce(new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * Random.Range(-_knifeBlewUpForce, _knifeBlewUpForce), ForceMode2D.Impulse);
                tp_Rigidbody2D.AddTorque(Random.Range(_knifeTorqueForce, _knifeTorqueForce), ForceMode2D.Impulse);
            }
        }
    }

    public Vector2 Time2ChangeDir {
        set {
            _time2ChangeDir = value;
            _timer2ChangeDir = Random.Range(_time2ChangeDir.x, _time2ChangeDir.y);
        }
    }

    public Vector2 Time2StopMove {
        set {
            _time2StopMove = value;
            _timer2StopMove = Random.Range(_time2StopMove.x, _time2StopMove.y);
        }
    }

    public Vector2 Time2StayStop {
        set {
            _time2StayStop = value;
            _timer2StayStop = Random.Range(_time2StayStop.x, _time2StayStop.y);
        }
    }

    public float RotateSpeed {
        set {
            _rotateSpeed = value;
            __rotateSpeedBackup = value;
            _rotateSpeedValue = value;
        }
    }
}
