﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apple : MonoBehaviour {

    [SerializeField]
    private SpriteRenderer spriteRenderer;
    private AudioSource _myAudioSource;

    [SerializeField]
    private Rigidbody2D[] _parts;

    [SerializeField]
    private float _time2Destroy = 3f;
    [SerializeField]
    private float _blewUpFor = 4f;
    private float _torqueForce = 6f;

    private void Awake() {
        _myAudioSource = GetComponent<AudioSource>();
    }

    void Update() {
        _time2Destroy -= Time.deltaTime;

        if (_time2Destroy < 0) {
            Destroy(gameObject);
        }
    }

    public void BlewUp() {
        _myAudioSource.Play();
        transform.parent = null;
        spriteRenderer.enabled = false;
        enabled = true;
        Destroy(GetComponent<Collider2D>());

        for (int i = 0; i < _parts.Length; i++) {
            _parts[i].gameObject.SetActive(true);
            _parts[i].AddForce(new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * Random.Range(-_blewUpFor, _blewUpFor), ForceMode2D.Impulse);
            _parts[i].AddTorque(Random.Range(-_torqueForce, _torqueForce), ForceMode2D.Impulse);
        }
    }
}
