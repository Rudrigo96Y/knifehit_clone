﻿using System.Collections;
using UnityEngine;

public class TwoImageEnable : MonoBehaviour {

    [SerializeField]
    private GameObject _toEnable;

    public bool SetEnable {
        set {
            _toEnable.SetActive(value);
        }
    }
}
