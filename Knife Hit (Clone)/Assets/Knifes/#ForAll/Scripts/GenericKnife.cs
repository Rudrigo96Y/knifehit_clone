﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PlayerPrefsEngine;
using GameManagerEngine;

public class GenericKnife : MonoBehaviour {

    private Rigidbody2D _myRigidbody2D;
    private AudioSource _myAudioSource;

    [SerializeField]
    private AudioClip _woodImpact;
    [SerializeField]
    private AudioClip _knifeImpact;

    private void Awake() {
        _myRigidbody2D = GetComponent<Rigidbody2D>();
        _myAudioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Enemy") {
            _myAudioSource.clip = _woodImpact;
            _myAudioSource.Play();

            if (GameManager.instance.GameHUDVar.CanThrow) {
                _myRigidbody2D.bodyType = RigidbodyType2D.Static;
                transform.parent = collision.gameObject.GetComponent<Stage0>().itensPos;
            } else {
                Destroy(GetComponent<Collider2D>());
                transform.eulerAngles = Vector3.zero;
                _myRigidbody2D.AddForce(Vector2.up * collision.relativeVelocity.magnitude, ForceMode2D.Impulse);
            }

            GameManager.instance.ReInstantiateKnife();
        } else if (collision.gameObject.tag == "Apple") {
            GameManager.instance.AddApple(1);
            transform.eulerAngles = Vector3.zero;
            _myRigidbody2D.AddForce(Vector2.up * collision.relativeVelocity.magnitude, ForceMode2D.Impulse);
            collision.gameObject.GetComponent<Apple>().BlewUp();
        } else if (collision.gameObject.tag == "GoldApple") {
            GameManager.instance.AddApple(3);
            transform.eulerAngles = Vector3.zero;
            _myRigidbody2D.AddForce(Vector2.up * collision.relativeVelocity.magnitude, ForceMode2D.Impulse);
            collision.gameObject.GetComponent<Apple>().BlewUp();
        } else {
            Destroy(GetComponent<Collider2D>());

            _myAudioSource.clip = _knifeImpact;
            _myAudioSource.Play();

            if (GameManager.instance.score > PlayerPrefsStr.HighestScore) {
                PlayerPrefsStr.HighestScore = GameManager.instance.score;
                PlayerPrefsStr.Stage = (GameManager.instance.stageIndice);
            }

            GameManager.instance.GameOver();
        }
    }
}
