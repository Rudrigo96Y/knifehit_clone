﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeEngine {

    public class KnifeEngineEnum {

        public enum KnifeClass {
            GetForApples,
            WatchVideos,
            BossKnives,
            ChallengeKnives
            //KnifePacks
        }
    }
}
