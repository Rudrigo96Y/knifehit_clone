﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerPrefsEngine {

    public class PlayerPrefsStr {

        static PlayerPrefsStr() {
            if (!PlayerPrefs.HasKey(_knifeEquiped)) {
                KnifeEquiped = 0;
            }
            if (!PlayerPrefs.HasKey(_apples)) {
                Apples = 0;
            }
            if (!PlayerPrefs.HasKey(_stage)) {
                Stage = 0;
            }
            if (!PlayerPrefs.HasKey(_highestScore)) {
                HighestScore = 0;
            }
            if (!PlayerPrefs.HasKey(_volume)) {
                Volume = 0f;
            }
            if (!PlayerPrefs.HasKey(_knife + "0")) {
                KnifeUnlock(0);
            }
        }

        private static string _knife = "Knife";
        private static string _apples = "Apples";
        private static string _knifeEquiped = "Knife";
        private static string _stage = "stage";
        private static string _highestScore = "HighestScore";
        private static string _volume = "Volume";

        public static int Apples {
            get {
                return PlayerPrefs.GetInt(_apples);
            }
            set {
                PlayerPrefs.SetInt(_apples, value);
            }
        }

        public static int KnifeEquiped {
            get {
                return PlayerPrefs.GetInt(_knifeEquiped);
            }
            set {
                PlayerPrefs.SetInt(_knifeEquiped, value);
            }
        }

        public static int Stage {
            get {
                return PlayerPrefs.GetInt(_stage);
            }
            set {
                PlayerPrefs.SetInt(_stage, value);
            }
        }

        public static int HighestScore {
            get {
                return PlayerPrefs.GetInt(_highestScore);
            }
            set {
                PlayerPrefs.SetInt(_highestScore, value);
            }
        }

        public static float Volume {
            get {
                return PlayerPrefs.GetFloat(_volume);
            }
            set {
                PlayerPrefs.SetFloat(_volume, value);
            }
        }

        public static bool KnifeUnlocked(int mth_indice) {
            return PlayerPrefs.HasKey(_knife + mth_indice.ToString());
        }

        public static void KnifeUnlock(int mth_indice) {
            PlayerPrefs.SetInt(_knife + mth_indice.ToString(), 1);
        }
    }
}
