﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

namespace GameManagerEngine {

    public class GameManagerWindow : EditorWindow {

        protected GameManager gameManager;
        protected GameObject gameManagerObj;
        protected Editor gameObjectEditor;

        #region Dados da janela
        private string _plusStr = "+";
        private string _doStr = "Do";
        private const float _PLUS_SPACE = 10f;
        private const float _DO_SPACE = 10f;
        private const float _FOLDOUTT_SCAPE = 16f;
        #endregion

        #region Dados do que esta editando no momento.
        private ThatIsEditing _thatIsEditing;
        private int _thatIsEditingIndice;
        private Stage _stageIsEditing;
        private Stage _bossIsEditing;
        private int _stageLevelIsEditing;
        #endregion

        [MenuItem("Window/Managers/Game Manager")]
        public static void ShowWindow() {
            //GetWindow(typeof(MyWindow));
            GetWindow<GameManagerWindow>("Game Manager");
        }

        private void OnGUI() {
            #region Para colocar o GameManager
            GUILayout.Label("GameManager:");
            gameManagerObj = (GameObject)EditorGUILayout.ObjectField(gameManagerObj, typeof(GameObject), false);

            //Valida se o obj não esta nullo.
            if (gameManagerObj != null) {
                //Valida se o obj colocado tem o script "GameManager".
                gameManager = gameManagerObj.GetComponent<GameManager>();
                if (gameManager == null) {
                    //Se não tiver mostra uma mensagem de erro.
                    Debug.LogError("O \"" + gameManagerObj.name + "\" não contem o script \"GameManager\"!");
                    gameManagerObj = null;
                    GUILayout.EndHorizontal();
                    return;
                }
            } else {
                string[] tp_guids1 = AssetDatabase.FindAssets("GameManager", null);
                foreach (string item in tp_guids1) {
                    GameManager tp_gameManager = (GameManager)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(item), typeof(GameManager));

                    if (tp_gameManager != null) {
                        gameManagerObj = tp_gameManager.gameObject;
                        GUILayout.EndHorizontal();
                        return;
                    }
                }
                GUILayout.EndHorizontal();
                return;
            }
            #endregion

            float tp_leftWidth = position.width * 0.3f;
            #region Lado esquerdo da tela, nessa parte fica os inimigos e bosses
            gameManager.leftScrollPos = GUILayout.BeginScrollView(gameManager.leftScrollPos, GUILayout.Width(tp_leftWidth));

            #region Estagios
            ShowStageLeftList("STAGE", "Stages", "Stage ", gameManager.stages, 4, ThatIsEditing.Stage, ref gameManager.showStages);
            ShowStageLeftList("BOSS", "Bosses", "Boss ", gameManager.bosses, 1, ThatIsEditing.Boss, ref gameManager.showBosses);

            #endregion
            GUILayout.EndScrollView();
            #endregion

            #region Lado direito da tela onde fica os dados da coisa aberta.
            GUILayout.BeginArea(new Rect(tp_leftWidth + 5f, 36f, position.width - (tp_leftWidth - 5f), position.height - 36f));
            gameManager.rightScrollPos = GUILayout.BeginScrollView(gameManager.rightScrollPos, GUILayout.Width(position.width - tp_leftWidth - 5f));
            if (_stageIsEditing != null) {
                ShowStageRight("STAGE ", gameManager.stages);
            } else if (_bossIsEditing != null) {
                ShowStageRight("BOSS ", gameManager.bosses);
            }
            GUILayout.EndScrollView();
            GUILayout.EndArea();
            #endregion
        }

        #region Mostrar as coisas
        private void ShowStageLeftList(string mth_header, string mth_FoldoutStr, string mth_tag, List<Stage> mth_stages, int mth_stageValuesLenght, ThatIsEditing mth_ThatIsEditing, ref bool ref_showStages) {
            GUILayout.Label(mth_header);
            GUILayout.BeginHorizontal();
            ref_showStages = EditorGUILayout.Foldout(ref_showStages, mth_FoldoutStr);
            if (GUILayout.Button(_plusStr)) {
                ref_showStages = true;
                GUILayout.Space(_PLUS_SPACE);
                mth_stages.Add(new Stage { stageValues = new StageValues[mth_stageValuesLenght] });
                SaveGameManagerPrefab();
            }
            GUILayout.EndHorizontal();

            if (ref_showStages) {
                for (int i = 0; i < mth_stages.Count; i++) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(_FOLDOUTT_SCAPE);
                    GUILayout.Label(mth_tag + i.ToString());
                    GUILayout.Space(_DO_SPACE);
                    if (GUILayout.Button(_doStr)) {
                        _thatIsEditingIndice = i;
                        _thatIsEditing = mth_ThatIsEditing;
                        ActionMenu.ShowActions("Open/Dell/Move", ActionCallBackBasic);
                    }
                    GUILayout.EndHorizontal();
                }
            }
        }

        private void ShowStageRight(string mth_header, List<Stage> mth_stages) {
            GUILayout.Label(mth_header + _thatIsEditingIndice);

            GameObject tp_wood = (GameObject)EditorGUILayout.ObjectField(mth_stages[_thatIsEditingIndice].wood, typeof(GameObject), false);

            GUILayout.Space(10f);
            GUILayout.BeginHorizontal();
            _stageLevelIsEditing %= mth_stages[_thatIsEditingIndice].stageValues.Length;
            for (int i = 0; i < mth_stages[_thatIsEditingIndice].stageValues.Length; i++) {
                if (_stageLevelIsEditing == i) {
                    if (GUILayout.Button("Level " + i.ToString())) {
                        _stageLevelIsEditing = i;
                    }
                } else if (GUILayout.Button("Level " + i.ToString(), EditorStyles.toolbarButton)) {
                    _stageLevelIsEditing = i;
                }
            }
            GUILayout.EndHorizontal();

            HorizontalGUILine();
            GUILayout.Label("Level: " + _stageLevelIsEditing.ToString());
            ShowStaveValues(mth_stages[_thatIsEditingIndice].stageValues[_stageLevelIsEditing]);
            HorizontalGUILine();

            if (tp_wood != mth_stages[_thatIsEditingIndice].wood) {
                mth_stages[_thatIsEditingIndice].wood = tp_wood;
                SaveGameManagerPrefab();
            }
        }

        private void ShowStaveValues(StageValues mth_stageValues) {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Knifes to Throw: ");
            Vector2 tp_knifes2Throw = EditorGUILayout.Vector2Field("", mth_stageValues.knifes2Throw);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("chances To Apple: ");
            Vector2 tp_chances2Apple = EditorGUILayout.Vector2Field("", mth_stageValues.chances2Apple);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Chances to Gold Apple: ");
            Vector2 tp_chances2GoldApple = EditorGUILayout.Vector2Field("", mth_stageValues.chances2GoldApple);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Chances to Knife Obstacle: ");
            Vector2 tp_chances2KnifeObstacle = EditorGUILayout.Vector2Field("", mth_stageValues.chances2KnifeObstacle);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Max Speed");
            Vector2 tp_maxSpeed = EditorGUILayout.Vector2Field("", mth_stageValues.maxSpeed);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Aceleration");
            Vector2 tp_aceleration = EditorGUILayout.Vector2Field("", mth_stageValues.aceleration);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Time To Change Dir");
            Vector2 tp_time2ChangeDir = EditorGUILayout.Vector2Field("", mth_stageValues.time2ChangeDir);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Time To Stop Move");
            Vector2 tp_time2StopMove = EditorGUILayout.Vector2Field("", mth_stageValues.time2StopMove);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Time To Stay Stop");
            Vector2 tp_time2StayStop = EditorGUILayout.Vector2Field("", mth_stageValues.time2StayStop);
            GUILayout.EndHorizontal();

            if (tp_knifes2Throw != mth_stageValues.knifes2Throw) {
                mth_stageValues.knifes2Throw = tp_knifes2Throw;
                SaveGameManagerPrefab();
            } else if (tp_chances2Apple != mth_stageValues.chances2Apple) {
                mth_stageValues.chances2Apple = tp_chances2Apple;
                SaveGameManagerPrefab();
            } else if (tp_chances2GoldApple != mth_stageValues.chances2GoldApple) {
                mth_stageValues.chances2GoldApple = tp_chances2GoldApple;
                SaveGameManagerPrefab();
            } else if (tp_chances2KnifeObstacle != mth_stageValues.chances2KnifeObstacle) {
                mth_stageValues.chances2KnifeObstacle = tp_chances2KnifeObstacle;
                SaveGameManagerPrefab();
            } else if (tp_maxSpeed != mth_stageValues.maxSpeed) {
                mth_stageValues.maxSpeed = tp_maxSpeed;
                SaveGameManagerPrefab();
            } else if (tp_aceleration != mth_stageValues.aceleration) {
                mth_stageValues.aceleration = tp_aceleration;
                SaveGameManagerPrefab();
            } else if (tp_time2ChangeDir != mth_stageValues.time2ChangeDir) {
                mth_stageValues.time2ChangeDir = tp_time2ChangeDir;
                SaveGameManagerPrefab();
            } else if (tp_time2StopMove != mth_stageValues.time2StopMove) {
                mth_stageValues.time2StopMove = tp_time2StopMove;
                SaveGameManagerPrefab();
            } else if (tp_time2StayStop != mth_stageValues.time2StayStop) {
                mth_stageValues.time2StayStop = tp_time2StayStop;
                SaveGameManagerPrefab();
            }
        }
        #endregion

        private void SaveGameManagerPrefab() {
            PrefabUtility.SavePrefabAsset(gameManager.gameObject);
        }

        private void HorizontalGUILine() {
            GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
        }

        #region CallBacks
        public void ActionCallBackBasic(object mth_value) {
            switch ((ActionType)mth_value) {
                case ActionType.Open:
                    ActionCallBackOpen();
                    break;
                case ActionType.Dell:
                    ActionCallBackDell();
                    break;
                case ActionType.Up:
                    ActionCallBackUp();
                    break;
                case ActionType.Down:
                    ActionCallBackDown();
                    break;
            }
        }

        public void ActionCallBackOpen() {
            _stageIsEditing = null;
            _bossIsEditing = null;

            switch (_thatIsEditing) {
                case ThatIsEditing.Stage:
                    _stageIsEditing = gameManager.stages[_thatIsEditingIndice];
                    break;
                case ThatIsEditing.Boss:
                    _bossIsEditing = gameManager.bosses[_thatIsEditingIndice];
                    break;
            }

            _thatIsEditing = ThatIsEditing.Null;
        }

        public void ActionCallBackDell() {
            switch (_thatIsEditing) {
                case ThatIsEditing.Stage:
                    gameManager.stages.RemoveAt(_thatIsEditingIndice);

                    if (gameManager.stages.Count == 0) {
                        gameManager.showStages = false;
                    } else if (_thatIsEditingIndice != 0) {
                        _thatIsEditingIndice--;
                    }
                    break;
                case ThatIsEditing.Boss:
                    gameManager.bosses.RemoveAt(_thatIsEditingIndice);

                    if (gameManager.bosses.Count == 0) {
                        gameManager.showBosses = false;
                    } else if (_thatIsEditingIndice != 0) {
                        _thatIsEditingIndice--;
                    }
                    break;
            }

            SaveGameManagerPrefab();
        }

        public void ActionCallBackUp() {
            Stage tp_stage;

            if (_thatIsEditingIndice > 0) {
                switch (_thatIsEditing) {
                    case ThatIsEditing.Stage:
                        tp_stage = gameManager.stages[_thatIsEditingIndice - 1];
                        gameManager.stages[_thatIsEditingIndice - 1] = gameManager.stages[_thatIsEditingIndice];
                        gameManager.stages[_thatIsEditingIndice] = tp_stage;
                        _thatIsEditingIndice--;
                        break;
                    case ThatIsEditing.Boss:
                        tp_stage = gameManager.bosses[_thatIsEditingIndice - 1];
                        gameManager.bosses[_thatIsEditingIndice - 1] = gameManager.bosses[_thatIsEditingIndice];
                        gameManager.bosses[_thatIsEditingIndice] = tp_stage;
                        _thatIsEditingIndice--;
                        break;
                }

                SaveGameManagerPrefab();
            }

            _thatIsEditing = ThatIsEditing.Null;
        }

        public void ActionCallBackDown() {
            Stage tp_stage;

            switch (_thatIsEditing) {
                case ThatIsEditing.Stage:
                    if (_thatIsEditingIndice < gameManager.stages.Count - 1) {
                        tp_stage = gameManager.stages[_thatIsEditingIndice + 1];
                        gameManager.stages[_thatIsEditingIndice + 1] = gameManager.stages[_thatIsEditingIndice];
                        gameManager.stages[_thatIsEditingIndice] = tp_stage;
                        _thatIsEditingIndice++;
                        SaveGameManagerPrefab();
                    }
                    break;
                case ThatIsEditing.Boss:
                    tp_stage = gameManager.bosses[_thatIsEditingIndice + 1];
                    gameManager.bosses[_thatIsEditingIndice + 1] = gameManager.bosses[_thatIsEditingIndice];
                    gameManager.bosses[_thatIsEditingIndice] = tp_stage;
                    _thatIsEditingIndice++;
                    SaveGameManagerPrefab();
                    break;
            }

            _thatIsEditing = ThatIsEditing.Null;
        }
        #endregion
    }

    public class ActionMenu {

        static GenericMenu _actionButton;

        /// <summary>
        /// Mostra o dropdown customizado.
        /// </summary>
        /// <param name="mth_noAdd">Nome dos itens a não serem mostrados.</param>
        /// /// <param name="mth_basic">CallBack.</param>
        public static void ShowActions(string mth_ToAdd, GenericMenu.MenuFunction2 mth_basic) {
            _actionButton = new GenericMenu();

            if (mth_ToAdd.Contains("Open")) {
                _actionButton.AddItem(new GUIContent("Open"), false, mth_basic, ActionType.Open);
            }
            if (mth_ToAdd.Contains("Dell")) {
                _actionButton.AddItem(new GUIContent("Dell"), false, mth_basic, ActionType.Dell);
            }
            if (mth_ToAdd.Contains("Move")) {
                _actionButton.AddItem(new GUIContent("Move/Up"), false, mth_basic, ActionType.Up);
                _actionButton.AddItem(new GUIContent("Move/Down"), false, mth_basic, ActionType.Down);
            }

            _actionButton.ShowAsContext();
        }
    }
}