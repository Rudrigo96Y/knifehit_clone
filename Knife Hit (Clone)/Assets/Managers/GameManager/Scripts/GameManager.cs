﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Audio;

using PlayerPrefsEngine;

namespace GameManagerEngine {

    public class GameManager : MonoBehaviour {

        public static GameManager instance;

        public AudioMixer audioMixer;

        public int score;
        public int stage;
        public int stageIndice;
        public int level;

        public List<GameObject> knifes;
        /// <summary>
        /// Quando chega no ultimo stagio volta anguns antes, pois os últimos são os com dificultade maior.
        /// </summary>
        public List<Stage> stages;
        public List<Stage> bosses;
        public int hardStageIndice;

        [Header("Pontos globais")]
        /// <summary>
        /// Posição onde a faca vai ser instanciada e lançada.
        /// </summary>
        [SerializeField]
        private Transform _knifeThrowPos;
        [SerializeField]
        private Transform _menuKnifePos;
        /// <summary>
        /// Posição de onde vai ficar os inimigos na tela, no caso as toras de madeira e os bosses.
        /// </summary>
        [SerializeField]
        private Transform _woodPos;

        [Header("Jogar faca")]
        public float throwForce;
        private Rigidbody2D _knife2Throw;

        [Header("Itens")]
        [SerializeField]
        private Item _apple;
        [SerializeField]
        private Item _goldApple;
        [SerializeField]
        private Item _knifeObstacle;
        [SerializeField]
        private float _itensRadius = 30;
        /// <summary>
        /// O jogo não pode ter mais de 18 maças.
        /// </summary>
        //[SerializeField]
        //private float _minAppledDis = 20;
        private Stage0 _stage;

        public GameHUD GameHUDVar { get; set; }

        [SerializeField]
        private float _time2NextLevel;
        private float _timer2NextLevel;
        [SerializeField]
        private float _gameOverTime;
        private float _gameOverTimer;

        #region Para a janela de edição do GameManager.
        [HideInInspector]
        public Vector2 leftScrollPos;
        [HideInInspector]
        public Vector2 rightScrollPos;
        [HideInInspector]
        public bool showStages;
        [HideInInspector]
        public bool showBosses;
        #endregion

        #region Métodos da própia Unity
        private void Awake() {
            audioMixer.SetFloat("Master", PlayerPrefsStr.Volume);

            _timer2NextLevel = -1;
            instance = this;
            CleanScene();
        }

        void Update() {
            if (_gameOverTimer > 0) {
                _gameOverTimer -= Time.deltaTime;

                if (_gameOverTimer <= 0) {
                    MainCanvas.instance.ShowPopup("GameOver");
                    enabled = false;
                    CleanScene();
                }
            } else if (Input.GetButtonDown("Fire1") && GameHUDVar.CanThrow) {
                if (_knife2Throw == null) {
                    InstantiateKnife();
                }

                GameHUDVar.HideLastKnifeCount();
                _knife2Throw.simulated = true;
                _knife2Throw.AddForce(Vector2.up * throwForce, ForceMode2D.Impulse);
                _knife2Throw = null;
            }

            if (_timer2NextLevel > 0) {
                _timer2NextLevel -= Time.deltaTime;

                if (_timer2NextLevel <= 0) {
                    NextLevel();
                }
            }
        }
        #endregion

        #region Prepara o jogo
        /// <summary>
        /// Explode os itens da tela.
        /// </summary>
        public void BlewUpItens() {
            _stage.BlewUp();

            _timer2NextLevel = _time2NextLevel;
            //Destroi todas as facas e só deixa a última, pois se clicar muito rápido da tempo de jogar outra faca.
            for (int i = 0; i < _knifeThrowPos.childCount - 1; i++) {
                Destroy(_knifeThrowPos.GetChild(i).gameObject);
            }
        }

        /// <summary>
        /// Instancia as coisas do próximo level.
        /// </summary>
        private void NextLevel() {
            level++;

            if (level > stages[stage].stageValues.Length) {
                level = 0;
                stage++;
                stageIndice++;

                if (stage > stages.Count - 1) {
                    stage = hardStageIndice;
                }

                GameHUDVar.ResetLevel();
                GameHUDVar.SetStage = stageIndice;
            }

            CleanScene();
            InstantiateKnife();

            if (level == stages[stage].stageValues.Length) {
                DestroyChilds(_woodPos);
                InstantiateBoss();
            } else {
                InstantiateWood();
            }

            GameHUDVar.NextLevel();
        }

        public void InstantiateWood() {
            _stage = GetWood(_woodPos).GetComponent<Stage0>();
            PrepareStage(stages[stage].stageValues[level]);

            //Guarda os ângulos já sorteados.
            int tp_maxI = GetIntRandom(stages[stage].stageValues[level].chances2Apple);
            for (int i = 0; i < tp_maxI; i++) {
                InstantiateItem(_stage, _apple);
            }
            tp_maxI = GetIntRandom(stages[stage].stageValues[level].chances2GoldApple);
            for (int i = 0; i < tp_maxI; i++) {
                InstantiateItem(_stage, _goldApple);
            }
            tp_maxI = GetIntRandom(stages[stage].stageValues[level].chances2KnifeObstacle);
            for (int i = 0; i < tp_maxI; i++) {
                InstantiateItem(_stage, _knifeObstacle);
            }
        }

        public void InstantiateBoss() {
            _stage = GetBoss(_woodPos, out int tp_randomValue).GetComponent<Stage0>();
            PrepareStage(bosses[tp_randomValue].stageValues[0]);

            //Guarda os ângulos já sorteados.
            int tp_maxI = GetIntRandom(stages[stage].stageValues[0].chances2Apple);
            for (int i = 0; i < tp_maxI; i++) {
                InstantiateItem(_stage, _apple);
            }
            tp_maxI = GetIntRandom(stages[stage].stageValues[0].chances2GoldApple);
            for (int i = 0; i < tp_maxI; i++) {
                InstantiateItem(_stage, _goldApple);
            }
            tp_maxI = GetIntRandom(stages[stage].stageValues[0].chances2KnifeObstacle);
            for (int i = 0; i < tp_maxI; i++) {
                InstantiateItem(_stage, _knifeObstacle);
            }
        }

        public void PrepareStage(StageValues mth_stageValues) {
            _stage.RotateSpeed = GetFloatRandom(mth_stageValues.maxSpeed) * (Random.Range(0, 2) == 0 ? -1 : 1);
            _stage.aceleration = GetFloatRandom(mth_stageValues.aceleration);
            _stage.Time2ChangeDir = mth_stageValues.time2ChangeDir;
            _stage.Time2StopMove = mth_stageValues.time2StopMove;
            _stage.Time2StayStop = mth_stageValues.time2StayStop;
        }

        public void InstantiateItem(Stage0 mth_stage, Item mth_item) {
            bool tp_contains;
            Transform tp_item = Instantiate(mth_item.item, mth_stage.itensPos).transform;
            int tp_count = 0;
            //Impedi que o ângulo sorteado fique muito perto do outro.
            do {
                tp_contains = false;

                tp_item.localEulerAngles = new Vector3(0f, 0f, Random.Range(0f, 360f));
                tp_item.localPosition = Vector3.zero;
                tp_item.position += tp_item.up * _itensRadius;

                for (int for_child = 0; for_child < mth_stage.itensPos.childCount - 1; for_child++) {
                    //Debug.Log(Quaternion.Angle(mth_stage.itensPos.GetChild(i).transform.rotation, tp_item.rotation));
                    if (Quaternion.Angle(mth_stage.itensPos.GetChild(for_child).transform.rotation, tp_item.rotation) < mth_item.minDis) {
                        tp_contains = true;
                        tp_item.localPosition = Vector3.zero;
                        break;
                    }
                }

                //Caso não consiga achar um lugar livre, o método é abortado.
                tp_count++;
                if (tp_count > 500) {
                    Destroy(tp_item.gameObject);
                    Debug.LogError("Loop possivelmente infinito!!!");
                    break;
                }
            } while (tp_contains);
        }

        /// <summary>
        /// Instancia a faca novamente na tela. Esse método deve ser usado quando a faca colidir com o inimigo.
        /// </summary>
        public void ReInstantiateKnife() {
            score++;
            GameHUDVar.SetScore = score;

            if (GameHUDVar.knife2Hide >= GameHUDVar.KnifeCount) {
                BlewUpItens();
            } else {
                InstantiateKnife();
            }
        }

        /// <summary>
        /// Instancia a faca na tela.
        /// </summary>
        public void InstantiateKnife() {
            _knife2Throw = GetKnifeEquiped(_knifeThrowPos).GetComponent<Rigidbody2D>();
            _knife2Throw.transform.localPosition = Vector3.zero;
        }

        /// <summary>
        /// Limpa a cena de jogo.
        /// </summary>
        public void CleanScene() {
            DestroyChilds(_knifeThrowPos);
            DestroyChilds(_woodPos);
        }

        /// <summary>
        /// Destroi os filhos do obj.
        /// </summary>
        /// <param name="mth_trans">Obj para destroir os filhos.</param>
        public void DestroyChilds(Transform mth_trans) {
            for (int i = 0; i < mth_trans.childCount; i++) {
                Destroy(mth_trans.GetChild(i).gameObject);
            }
        }

        public void GameOver() {
            _gameOverTimer = _gameOverTime;
        }
        #endregion

        #region Prepara o menu
        public void InstantiateMenuKnife() {
            DestroyChilds(_menuKnifePos);

            Transform tp_trans = Instantiate(knifes[PlayerPrefsStr.KnifeEquiped], _menuKnifePos).transform;
            tp_trans.localPosition = Vector3.zero;
        }
        #endregion

        #region Pegar obj para o jogo
        public GameObject GetKnifeEquiped(Transform mth_parent) {
            return Instantiate(knifes[PlayerPrefsStr.KnifeEquiped], mth_parent);
        }

        public GameObject GetWood(Transform mth_parent) {
            return Instantiate(stages[stage].wood, mth_parent);
        }

        public GameObject GetBoss(Transform mth_parent, out int out_randomValue) {
            out_randomValue = Random.Range(0, bosses.Count);
            return Instantiate(bosses[out_randomValue].wood, mth_parent);
        }

        //public GameObject GetBoss(Transform mth_parent) {
        //    return Instantiate(levels[level].boss, mth_parent);
        //}

        public int Getknifes2Throw() {
            //Se o level for maior quer dizer que é boss.
            if (level > stages[stage].stageValues.Length - 1) {
                return GetIntRandom(stages[stage].stageValues[0].knifes2Throw);
            } else {
                return GetIntRandom(stages[stage].stageValues[level].knifes2Throw);
            }
        }

        public float GetMaxSpeed() {
            return GetFloatRandom(stages[stage].stageValues[level].maxSpeed);
        }

        public int GetIntRandom(Vector2 mth_Vector2) {
            return Random.Range((int)mth_Vector2.x, (int)mth_Vector2.y);
        }

        public float GetFloatRandom(Vector2 mth_Vector2) {
            return Random.Range(mth_Vector2.x, mth_Vector2.y);
        }
        #endregion

        #region Pontuação e coisas do gênero
        public void AddApple(int mth_value) {
            PlayerPrefsStr.Apples += mth_value;
            FindObjectOfType<MainCanvas>().SetApple = PlayerPrefsStr.Apples;
        }
        #endregion
    }

    [System.Serializable]
    public class Item {

        public GameObject item;
        public float minDis;
    }

    [System.Serializable]
    public class Stage {

        public GameObject wood;
        public StageValues[] stageValues;
    }

    [System.Serializable]
    public class StageValues {
        /// <summary>
        /// x: Menor valor para o random
        /// y: Maior valor para o random
        /// </summary>
        public Vector2 knifes2Throw;
        /// <summary>
        /// x: Menor valor para o random
        /// y: Maior valor para o random
        /// </summary>
        public Vector2 chances2Apple;
        /// <summary>
        /// x: Menor valor para o random
        /// y: Maior valor para o random
        /// </summary>
        public Vector2 chances2GoldApple;
        /// <summary>
        /// x: Menor valor para o random
        /// y: Maior valor para o random
        /// </summary>
        public Vector2 chances2KnifeObstacle;
        /// <summary>
        /// x: Menor valor para o random
        /// y: Maior valor para o random
        /// </summary>
        public Vector2 maxSpeed;
        /// <summary>
        /// x: Menor valor para o random
        /// y: Maior valor para o random
        /// </summary>
        public Vector2 aceleration;
        /// <summary>
        /// x: Menor valor para o random
        /// y: Maior valor para o random
        /// <= 0 não muda a rotação
        /// </summary>
        public Vector2 time2ChangeDir;
        /// <summary>
        /// x: Menor valor para o random
        /// y: Maior valor para o random
        /// <= 0 não muda a rotação
        /// </summary>
        public Vector2 time2StopMove;
        /// <summary>
        /// x: Menor valor para o random
        /// y: Maior valor para o random
        /// </summary>
        public Vector2 time2StayStop;
    }
}
