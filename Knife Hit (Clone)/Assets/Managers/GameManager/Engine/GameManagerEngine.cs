﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameManagerEngine {

    public enum ThatIsEditing {
        Stage,
        Boss,
        Null,
    }

    public enum ActionType {
        Open,
        Dell,
        Up,
        Down,
        Null,
    }
}
