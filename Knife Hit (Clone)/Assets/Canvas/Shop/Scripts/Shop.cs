﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameManagerEngine;
using PlayerPrefsEngine;

using UnityEngine.UI;

public class Shop : Popup {

    private string _scene2Back;
    
    [SerializeField]
    private Image _butButton;

    [SerializeField]
    private Transform _knifeShopPos;
    [SerializeField]
    private GameObject _shopItemPrefab;

    [SerializeField]
    private Transform _normalKnife;
    [SerializeField]
    private Transform _videoKnife;
    [SerializeField]
    private Transform _bossKnife;

    private int _indiceSelected;

    public override void MyStart() {
        base.MyStart();

        DestroyChilds(_normalKnife);
        DestroyChilds(_knifeShopPos);
        DestroyChilds(_videoKnife);

        for (int i = 0; i < GameManager.instance.knifes.Count; i++) {
            ShopItem tp_ShopItem = Instantiate(_shopItemPrefab, _normalKnife).GetComponent<ShopItem>();
            tp_ShopItem.indice = i;
            tp_ShopItem.sprite.sprite = GameManager.instance.knifes[i].GetComponentInChildren<SpriteRenderer>().sprite;
            
            if (!PlayerPrefsStr.KnifeUnlocked(i)) {
                tp_ShopItem.sprite.color = new Color(0.1f, 0.1f, 0.1f);
            }
        }

    }

    public override void ShowMe() {
        base.ShowMe();
        _indiceSelected = PlayerPrefsStr.KnifeEquiped;
        GameManager.instance.GetKnifeEquiped(_knifeShopPos).transform.localPosition = Vector3.zero;
        _butButton.color = new Color(_butButton.color.r, _butButton.color.g, _butButton.color.b, 0.3f);
    }

    public void InstantiateKnife(int mth_indice) {
        _indiceSelected = mth_indice;
        DestroyChilds(_knifeShopPos);
        Instantiate(GameManager.instance.knifes[_indiceSelected], _knifeShopPos).transform.localPosition = Vector3.zero;

        if (PlayerPrefsStr.KnifeUnlocked(_indiceSelected)) {
            _butButton.color = new Color(_butButton.color.r, _butButton.color.g, _butButton.color.b, 0.3f);
        } else {
            _butButton.color = new Color(_butButton.color.r, _butButton.color.g, _butButton.color.b, 1f);
        }
    }

    public void SetScene2Back(string mth_value) {
        _scene2Back = mth_value;
    }

    public void Back() {
        MainCanvas.instance.ShowPopup(_scene2Back);
    }

    public void DestroyChilds(Transform mth_trans) {
        for (int i = 0; i < mth_trans.childCount; i++) {
            Destroy(mth_trans.GetChild(i).gameObject);
        }
    }

    public void BuyRandom() {
        if (PlayerPrefsStr.Apples >= 50) {
            List<int> tp_toUnlock = new List<int>();
            for (int i = 0; i < GameManager.instance.knifes.Count; i++) {
                if (!PlayerPrefsStr.KnifeUnlocked(i)) {
                    tp_toUnlock.Add(i);
                }
            }

            if (tp_toUnlock.Count != 0) {
                _indiceSelected = tp_toUnlock[Random.Range(0, tp_toUnlock.Count)];
                PlayerPrefsStr.KnifeUnlock(_indiceSelected);
                _normalKnife.GetChild(_indiceSelected).GetComponent<ShopItem>().sprite.color = Color.white;
                PlayerPrefsStr.Apples -= 50;
                MainCanvas.instance.SetApple = PlayerPrefsStr.Apples;
            }
        }
    }

    public void Buy() {
        if (!PlayerPrefsStr.KnifeUnlocked(_indiceSelected) && PlayerPrefsStr.Apples >= 150) {
            PlayerPrefsStr.KnifeUnlock(_indiceSelected);
            PlayerPrefsStr.KnifeEquiped = _indiceSelected;
            _normalKnife.GetChild(_indiceSelected).GetComponent<ShopItem>().sprite.color = Color.white;
            PlayerPrefsStr.Apples -= 150;
            MainCanvas.instance.SetApple = PlayerPrefsStr.Apples;
        }
    }

    public void Video() {
        //PlayerPrefsStr.Apples += 25;
    }
}
