﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour {

    private Shop parent_Shop;

    public int indice;
    public Image sprite;

    private void Awake() {
        parent_Shop = GetComponentInParent<Shop>();
    }

    public void SelectMe() {
        parent_Shop.InstantiateKnife(indice);
    }
}
