﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using GameManagerEngine;
using PlayerPrefsEngine;

public class Menu : Popup {

    [SerializeField]
    private Text _stageText;
    [SerializeField]
    private Text _scoreText;

    public override void ShowMe() {
        GameManager.instance.enabled = false;
        GameManager.instance.InstantiateMenuKnife();

        _stageText.text = "STAGE " + PlayerPrefsStr.Stage;
        _scoreText.text = "SCORE " + PlayerPrefsStr.HighestScore;

        base.ShowMe();
    }

    public int SetStage {
        set {
            _stageText.text = value.ToString();
        }
    }

    public int SetScore {
        set {
            _scoreText.text = value.ToString();
        }
    }
}
