﻿using GameManagerEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class GameOver : Popup {

    [SerializeField]
    private Text _score;
    [SerializeField]
    private Text _stage;

    public override void ShowMe() {
        _score.text = GameManager.instance.score.ToString();
        _stage.text = "STAGE: " + (GameManager.instance.stage + 1).ToString();

        base.ShowMe();
    }

    public void Restart() {
        MainCanvas.instance.ShowPopup("GameHUD");
    }
}
