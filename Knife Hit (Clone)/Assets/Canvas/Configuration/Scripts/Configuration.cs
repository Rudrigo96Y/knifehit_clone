﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Audio;
using UnityEngine.UI;

using GameManagerEngine;
using PlayerPrefsEngine;

public class Configuration : Popup {

    private AudioSource _myAudioSource;

    [SerializeField]
    private Slider _volume;

    private string _scene2Back;

    public override void MyStart() {
        _myAudioSource = GetComponent<AudioSource>();
        base.MyStart();
    }

    public override void ShowMe() {
        _volume.value = PlayerPrefsStr.Volume;
        base.ShowMe();
    }

    public void SaveVolume() {
        GameManager.instance.audioMixer.SetFloat("Master", _volume.value);
        PlayerPrefsStr.Volume = _volume.value;

        if (!_myAudioSource.isPlaying) {
            _myAudioSource.Play();
        }
    }

    public void SetScene2Back(string mth_value) {
        _scene2Back = mth_value;
    }

    public void Back() {
        MainCanvas.instance.ShowPopup(_scene2Back);
    }
}
