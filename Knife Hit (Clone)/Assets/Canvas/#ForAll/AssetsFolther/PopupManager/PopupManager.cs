﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : MonoBehaviour {

    [SerializeField]
    protected int popupEnable;
    [SerializeField]
    protected Popup[] popups;

    protected virtual void Start() {
        HideAllPopups();
        ShowPopup();
    }

    public void ShowPopup() {
        popups[popupEnable].ShowMe();
    }

    public void ShowPopup(int mth_indice) {
        popups[popupEnable].HideMe();
        popups[mth_indice].ShowMe();
        popupEnable = mth_indice;
    }

    public void ShowPopup(string mth_name) {
        popups[popupEnable].HideMe();

        for (int i = 0; i < popups.Length; i++) {
            if (popups[i].gameObject.name == mth_name) {
                popups[i].ShowMe();
                popupEnable = i;
            }
        }
    }

    public void HidePopup() {
        popups[popupEnable].HideMe();
    }

    public void HidePopup(int mth_indice) {
        popups[mth_indice].HideMe();
    }

    public void HideAllPopups() {
        foreach (Popup item in popups) {
            item.HideMe();
        }
    }
}
