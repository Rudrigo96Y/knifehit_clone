﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popup : MonoBehaviour {

    [SerializeField]
    protected GameObject[] _obj2ChangeEnable;

    public virtual void MyStart() {}

    public virtual void ShowMe() {
        foreach (GameObject item in _obj2ChangeEnable) {
            item.SetActive(true);
        }

        gameObject.SetActive(true);
    }

    public virtual void HideMe() {
        foreach (GameObject item in _obj2ChangeEnable) {
            item.SetActive(false);
        }

        gameObject.SetActive(false);
    }
}
