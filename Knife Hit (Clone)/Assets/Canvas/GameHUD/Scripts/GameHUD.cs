﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using GameManagerEngine;

public class GameHUD : Popup {

    private TwoImageEnable[] _childTwoImageEnable;

    public int knife2Hide;
    [SerializeField]
    private Transform _knifeCount;
    [SerializeField]
    private GameObject _knifeCountPrefab;
    [SerializeField]
    private Text _score;
    [SerializeField]
    private Text _stage;

    public override void MyStart() {
        GameManager.instance.GameHUDVar = this;
        _childTwoImageEnable = GetComponentsInChildren<TwoImageEnable>();

        base.MyStart();
    }

    public override void ShowMe() {
        base.ShowMe();

        GameManager.instance.score = 0;
        GameManager.instance.stage = 0;
        GameManager.instance.level = 0;
        GameManager.instance.stageIndice = 1;

        SetStage = GameManager.instance.stageIndice;

        CleanKnifeCount();
        InstantiateKnifeCount();
        ResetLevel();

        GameManager.instance.CleanScene();
        GameManager.instance.InstantiateKnife();
        GameManager.instance.InstantiateWood();
        GameManager.instance.enabled = true;

        SetScore = GameManager.instance.score;
    }

    public void NextLevel() {
        if (GameManager.instance.level < _childTwoImageEnable.Length) {
            _childTwoImageEnable[GameManager.instance.level].SetEnable = true;
        }

        CleanKnifeCount();
        InstantiateKnifeCount();
    }

    public void HideLastKnifeCount() {
        _knifeCount.GetChild(knife2Hide).GetComponent<TwoImageEnable>().SetEnable = false;
        knife2Hide++;
    }

    public void InstantiateKnifeCount() {
        knife2Hide = 0;
        for (int i = 0; i < GameManager.instance.Getknifes2Throw(); i++) {
            Instantiate(_knifeCountPrefab, _knifeCount);
        }
    }

    public void CleanKnifeCount() {
        for (int i = 0; i < _knifeCount.childCount; i++) {
            Destroy(_knifeCount.GetChild(i).gameObject);
        }
    }

    public void ResetLevel() {
        _childTwoImageEnable[0].SetEnable = true;
        for (int i = 1; i < _childTwoImageEnable.Length; i++) {
            _childTwoImageEnable[i].SetEnable = false;
        }
    }

    /// <summary>
    /// Retorna a quantidade de filhos que o "_knifeCount" tem.
    /// </summary>
    public int KnifeCount {
        get {
            return _knifeCount.childCount;
        }
    }

    public int SetScore {
        set {
            _score.text = value.ToString();
        }
    }

    public int SetStage {
        set {
            _stage.text = "STAGE " + value.ToString();
        }
    }

    public bool CanThrow {
        get {
            return knife2Hide < _knifeCount.childCount;
        }
    }
}
