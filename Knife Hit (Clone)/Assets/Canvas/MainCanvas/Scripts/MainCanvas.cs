﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using PlayerPrefsEngine;

public class MainCanvas : PopupManager {

    public static MainCanvas instance;

    [SerializeField]
    private Text _appleText;

    private void Awake() {
        instance = this;
        _appleText.text = PlayerPrefsStr.Apples.ToString();
    }
    protected override void Start() {
        foreach (var item in popups) {
            item.MyStart();
        }

        base.Start();
    }

    public int SetApple {
        set {
            _appleText.text = value.ToString();
        }
    }
}
